import qrcode
import re

#QR Code Setting
qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_M,
    box_size=2,
    border=4,
)

#Input Inventory Information
print("="*50)
print('Please Input Your Information inventory')
data = input('')
qr.add_data(data)
qr.make(fit=True)


#Input File Name
img = qr.make_image(fill='black', back_color='white')
print("="*50)
print('Please Input Your QR Code File Name')
imgname = input('')

#Validation File Name
regex = re.compile('[@_!#$%^&*()<>?/\|}{~:]')
if(regex.search(imgname) == None):
    print("="*50)
    print("File Name is accepted")
    print("="*50)
    img = qr.make_image(fill='black', back_color='white')
    imgpath = 'QRcode_img/'
    img.save(imgpath+imgname)       
else:
    print("="*50)
    print("File Name is not accepted.")
    print("="*50)
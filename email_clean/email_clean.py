import datetime
import argparse
import logging
import imaplib
from datetime import timedelta
from imap_tools import MailBox, AND

log = logging.getLogger('Inbox-Mail-Delete')
log.setLevel('DEBUG')

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

log.addHandler(ch)


def main():
    parser = argparse.ArgumentParser(description="Automate Mail Delete")
    parser.add_argument("--weeks", metavar="weeks", type=int, default=0, help="Week")
    parser.add_argument("--days", metavar="days", type=int, default=0, help="Days")
    parser.add_argument("--date", metavar="date", type=str, help="Date")
    parser.add_argument("--delete", metavar="delete", type=__str2bool, default=False, help="Delete?")
    parser.add_argument("--verbose", action="store_true", help="Set level to verbose")

    args = parser.parse_args()
    weeks = args.weeks
    days = args.days
    date = args.date
    delete = args.delete

    if not date:
        delta = timedelta(weeks=weeks, days=days)
        now = datetime.date.today()
        lte = now - delta
    else:
        lte = datetime.date.strftime(date, '%Y-%m-%d')

    log.info(lte)

    mailbox = MailBox(host='put_email_server')
    mailbox.login('put_email_adress', 'put_email_pass', initial_folder='put_email_folder')
    for mail in mailbox.fetch(AND(date_lt=lte, from_='put_email_subject')):
        log.info(mail.subject)
        if delete:
            mail_delete = mailbox.delete(mail.uid)
            log.info(mail_delete)


def __str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1', 1):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0', 0):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        log.info('Got keyboard interupt, exiting')